
var ContrastProgress = {

    SettingsJsonObject: {},

    initialise: function (){
        ContrastProgress.loadSettingsJson();
    },

    loadSettingsJson: function (){
        var settingsFileUrl = "src/settings/ProgressBarSettings.json";
        $.getJSON(settingsFileUrl,function (json) {
            ContrastProgress.SettingsJsonObject = json;
            if(json.isEnabled){
                ContrastProgress.createContrastTimeline();
            }
        });
    },

    createContrastTimeline: function (){
        ContrastProgress.cloneDivPrependTo('jsNormalTimeline','jsContrastTimeline','jsTimelineContainer');
        ContrastProgress.addInternalDiv();
        ContrastProgress.setInitialCssProperties();
        ContrastProgress.setIE7Compatibility();
    },

    addInternalDiv: function () {
        $('#jsContrastTimeline').wrapInner("<div id='jsContrastFixedWidth'></div>");
    },

    cloneDivPrependTo: function (divId, newDivId, prependToDivID) {
        $('#' + divId).clone(true).prop('id', newDivId).prependTo('#' + prependToDivID);
    },

    setInitialCssProperties: function () {
        $('#jsContrastTimeline').css({
            "position": "absolute",
            "width": "0",
            "color": ContrastProgress.SettingsJsonObject.fontColor,
            "background-color": ContrastProgress.SettingsJsonObject.backgroundColor,
            "background": "url(" + ContrastProgress.SettingsJsonObject.backgroundImagePath + ") bottom left repeat-x",
            "overflow": "hidden",
            "z-index": "2",
            "left": "0"
        });
        $('#jsContrastFixedWidth').css('width',$('#jsNormalTimeline').width() + "px");
        $('#jsTimelineProgressHover').css('background', 'url("' + ContrastProgress.SettingsJsonObject.hoverProgressImagePath + '")');
        $('#jsTimelineProgressHover').css('background-color', ContrastProgress.SettingsJsonObject.hoverProgressColor);
    },

    setIE7Compatibility: function () {
        if($('html').hasClass('ie7')){
            $("#jsContrastTimeline").css({
                "border-right": "1px solid #666",
                "border-left": "1px solid #666"
            });
            $("#jsTimelineProgress").detach().insertAfter("#jsTimelineContainer");
            $("#jsTimelineProgress").css("background", "");
            $("#jsTimelineProgressHover").css("background", "");
        }
    },

    setContrastTimelineProgress: function (progress){
        var timeline_width = $('#jsTimelineContainer').width();
        if($('html').hasClass('ie7')){
            $('#jsContrastTimeline').width((timeline_width * progress)-2);
        } else {
            $('#jsContrastTimeline').width(timeline_width * progress);
        }

    }
};